package assignment3;

@FunctionalInterface
interface Inter1{
	abstract int cube(int n);
	default int square(int n)
	{
		return n*n;
	}
	default int add(int a,int b)
	{
		return a+b;
	}
	static int sub(int a,int b)
	{
		return a-b;
	}
	static int mul(int a,int b)
	{
		return a*b;
	}
	static double div(int a,int b)
	{
		return ((double)a/(double)b);
	}
}

public class Inter2 implements Inter1{

	@Override
	public int cube(int n) {
		// TODO Auto-generated method stub
		return n*n*n;
	}
	
	public static void main(String args[])
	{
		Inter2 obj = new Inter2();
		System.out.println("Cube: "+obj.cube(3));
		System.out.println("Square: "+obj.square(3));
		System.out.println("Add: "+obj.add(43,12));
		System.out.println("Sub: "+Inter1.sub(43,12));
		System.out.println("Mul: "+Inter1.mul(43,12));
		System.out.format("Div: %.2f",Inter1.div(43,12));
		
	}

}
